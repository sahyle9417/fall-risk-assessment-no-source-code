# SHLee's Multivariate Time Series Classification

## Requirements
- Conda
- Python 3.5 (On Python 3.6, Model Cannot Recognize GPU) 
- Keras with Tensorflow Backend
- Pandas
- Sklearn
- Matplotlib
- Opencv

## Installation
- conda create -n tf35[gpu] python=3.5
- source activate tf35[gpu]
- pip install pandas
- pip install sklearn
- pip install matplotlib
- pip install tensorflow[-gpu]
- pip install keras
- conda install -c conda-forge opencv

## How to Train
- Make Commands : make what
- Preprocess : make pr
- For Using GPU, Set GPU_ID_LIST in 'train_{model_name}.py'
- Train Basic LSTM Model : make lstm
- Train FCN Model : make fcn
- Train FCN-Conv2D Model: make fcn_conv2d
- Train LSTM-FCN Model : make lstm_fcn
- Train LSTM-FCN-Conv2D Model: make lstm_fcn_conv2d
- Train Attention-LSTM-FCN Model: make alstm_fcn
- Train Attention-LSTM-FCN-Conv2D Model: make alstm_fcn_conv2d
- Train CNN-LSTM-FCN Model: make cnn_lstm_fcn
- Train CNN-LSTM-FCN-Conv2D Model: make cnn_lstm_fcn_conv2d

## Input Data Description
- 14 BBS Actions
- 50 Subjects * 3 Trials = 150 Samples
- 11 Sensors * 12 Features in Each Timestep -> 132 Features in Each Timestep
- Each Timestep Represents 1/60 seconds (60Hz)
- 12 Features : Acc_X, Acc_Y ,Acc_Z, Gyr_X, Gyr_Y, Gyr_Z, Mag_X, Mag_Y, Mag_Z, Roll, Pitch, Yaw
- For Roll Pitch Yaw Only (33 Features), Set "RPY_ONLY" to True in train_{model_name}.py File
- For Disable MinMax Scaling, Set "INPUT_MINMAX_SCALE" to False" in train_{model_name}.py File

## Model Description

### Basic LSTM
- Train by 'make lstm_fcn' or 'python3 train_lstm.py'
- Implemented in models.py -> load_lstm()

### FCN
- Train by 'make fcn' or 'python3 train_fcn.py'
- Implemented in models.py -> load_fcn()
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### FCN-Conv2D
- Train by 'make fcn_conv2d' or 'python3 train_fcn_conv2d.py'
- Implemented in models.py -> load_fcn_conv2d()
- FCN-Conv2D is Conv2D version of FCN Model
- Setting Kernel Width and Stride Width to 12 To Combine Single Sensor Data's Features
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### LSTM-FCN
- Train by 'make lstm_fcn' or 'python3 train_lstm_fcn.py'
- Implemented in models.py -> load_lstm_fcn()
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### LSTM-FCN-Conv2D
- Train by 'make lstm_fcn_conv2d' or 'python3 train_lstm_fcn_conv2d.py'
- Implemented in models.py -> load_lstm_fcn_conv2d()
- LSTM-FCN-Conv2D is Conv2D version of LSTM-FCN Model
- Setting Kernel Width and Stride Width to 12 To Combine Single Sensor Data's Features
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### LSTM-No-Permute-FCN
- Train by 'make lstm_no_permute_fcn' or 'python3 train_lstm_no_permute_fcn.py'
- Implemented in models.py -> load_lstm_no_permute_fcn()
- LSTM-No-Permute-FCN has No Permutation(Dimension Shuffle) Before LSTM
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### LSTM-No-Permute-FCN-Conv2D
- Train by 'make lstm_no_permute_fcn_conv2d' or 'python3 train_lstm_no_permute_fcn_conv2d.py'
- Implemented in models.py -> load_lstm_fcn_conv2d()
- LSTM-No-Permute-FCN-Conv2D is Conv2D version of LSTM-No-Permute-FCN Model
- Setting Kernel Width and Stride Width to 12 To Combine Single Sensor Data's Features
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### Attention-LSTM-FCN
- Train by 'make alstm_fcn' or 'python3 train_alstm_fcn.py'
- Implemented in models.py -> load_alstm_fcn()
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### Attention-LSTM-FCN-Conv2D
- Train by 'make alstm_fcn_conv2d' or 'python3 train_alstm_fcn_conv2d.py'
- Implemented in models.py -> load_alstm_fcn_conv2d()
- Attention-LSTM-FCN-Conv2D is Conv2D version of Attention-LSTM-FCN Model
- Setting Kernel Width and Stride Width to 12 To Combine Single Sensor Data's Features
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### Attention-LSTM-No-Permute-FCN
- Train by 'make alstm_no_permute_fcn' or 'python3 train_alstm_no_permute_fcn.py'
- Implemented in models.py -> load_alstm_no_permute_fcn()
- Attention-LSTM-No-Permute-FCN has No Permutation(Dimension Shuffle) Before LSTM
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### Attention-LSTM-No-Permute-FCN-Conv2D
- Train by 'make alstm_fcn_conv2d' or 'python3 train_alstm_fcn_conv2d.py'
- Implemented in models.py -> load_alstm_fcn_conv2d()
- Attention-LSTM-No-Permute-FCN-Conv2D is Conv2D version of Attention-LSTM-No-Permute-FCN Model
- Setting Kernel Width and Stride Width to 12 To Combine Single Sensor Data's Features
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### CNN-LSTM
- Train by 'make cnn_lstm' or 'python3 train_cnn_lstm.py'
- Implemented in models.py -> load_cnn_lstm()
- CNN-LSTM Reference : https://machinelearningmastery.com/how-to-develop-rnn-models-for-human-activity-recognition-time-series-classification/

### CNN-LSTM-FCN
- Train by 'make cnn_lstm_fcn' or 'python3 train_cnn_lstm_fcn.py'
- Implemented in models.py -> load_cnn_lstm_fcn()
- CNN-LSTM-FCN is Combination of CNN-LSTM Model and LSTM-FCN Model
- CNN-LSTM Reference : https://machinelearningmastery.com/how-to-develop-rnn-models-for-human-activity-recognition-time-series-classification/
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

### CNN-LSTM-FCN-Conv2D
- Train by 'make cnn_lstm_fcn_conv2d' or 'python3 train_cnn_lstm_fcn_conv2d.py'
- Implemented in models.py -> load_cnn_lstm_fcn_conv2d()
- CNN-LSTM-FCN-Conv2D is Conv2D version of CNN-LSTM-FCN Model
- Setting Kernel Width and Stride Width to 12 To Combine Single Sensor Data's Features
- CNN-LSTM Reference : https://machinelearningmastery.com/how-to-develop-rnn-models-for-human-activity-recognition-time-series-classification/
- LSTM-FCN Reference Paper : https://arxiv.org/abs/1801.04503
- LSTM-FCN Reference Github : https://github.com/titu1994/MLSTM-FCN

## File Description

### preprocess.py
- Load Raw 132 Sensor Features
- Save All Features(132) to OUTPUT_ALL_DIR
- Save Roll, Pitch, Yaw Features(33) to OUTPUT_RPY_DIR

### models.py
- load_lstm() : Implements Basic LSTM
- load_fcn() : Implements FCN
- load_fcn_conv2d() : Implements FCN-Conv2D
- load_lstm_fcn() : Implements LSTM-FCN
- load_lstm_fcn_conv2d() : Implements LSTM-FCN-Conv2D
- load_lstm_no_permute_fcn() : Implements LSTM-No-Permute-FCN
- load_lstm_no_permute_fcn_conv2d() : Implements LSTM-No-Permute-FCN-Conv2D
- load_alstm_fcn() : Implements Attention-LSTM-FCN
- load_alstm_fcn_conv2d() : Implements Attention-LSTM-FCN-Conv2D
- load_alstm_no_permute_fcn() : Implements Attention-LSTM-No-Permute-FCN
- load_alstm_no_permute_fcn_conv2d() : Implements Attention-LSTM-No-Permute-FCN-Conv2D
- load_cnn_lstm() : Implements CNN-LSTM
- load_cnn_lstm_fcn() : Implements CNN-LSTM-FCN
- load_cnn_lstm_fcn_conv2d() : Implements CNN-LSTM-FCN-Conv2D
- squeeze_excite_block() : Implements Squeeze-and-Excitation Networks
- AttentionLSTM() : Implements Attention LSTM by Overriding

### train_{model_name}.py
- Load Preprocessed Data
- Load Model from models.py using load_{model_name}()
- For Model Description and Reference, Check README.md -> Model Description
- Train Model
- Calculate and Evaluate Argmax Prediction (Argmax of Softmax Output)
- Calculate and Evaluate Weighted Prediction (Weighted Sum of Softmax Output)
- All Log is Printed on Console and Dumped into Result Directory at the Same Time
- Dumped Log Filename is at ./Result/{model_name}_result.txt

### common_functions.py
- Implements Common Functions Used in train_*.py Files
- get_session() : GPU Configuration
- evaluate_prediction() : Evaluate Model's Softmax Output
- calculate_root_mse() : Calculate Root of MSE of Model's Softmax Output (Lower is Better)
- calculate_accuracy() : Calculate Accuracy of Model's Output (Higher is Better)
- round_arr() : Round Weighted Prediction for Calculating Accuracy (Because Cannot Compare Float and Int)
