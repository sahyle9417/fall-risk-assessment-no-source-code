# Uses python3.5
# Requires pandas, sklearn, tensorflow, keras, opencv

OUTPUT_RPY_DIR = './Data/SungNam_Elderly_Preprocess_RPY'
OUTPUT_ALL_DIR = './Data/SungNam_Elderly_Preprocess'

what:
	@echo "Python3.5 Recommended"
	@echo "Before Training, You Must Preprocess Input Data by 'make pr'"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	@echo "	pr: Preprocess All Feature(132) and Roll, Pitch, Yaw(33) Separately"
	@echo "	lstm: Train LSTM Model"
	@echo "	fcn: Train FCN Model"
	@echo "	fcn_conv2d: Train FCN-Conv2D Model"
	@echo "	lstm_fcn: Train LSTM-FCN Model"
	@echo "	lstm_fcn_conv2d: Train LSTM-FCN-Conv2D Model"
	@echo "	lstm_no_permute_fcn: Train LSTM-No-Permute-FCN Model"
	@echo "	lstm_no_permute_fcn_conv2d: Train LSTM-No-Permute-FCN-Conv2D Model"
	@echo "	alstm_fcn: Train Attention-LSTM-FCN Model"
	@echo "	alstm_fcn_conv2d: Train Attention-LSTM-FCN-Conv2D Model"
	@echo "	alstm_no_permute_fcn: Train Attention-LSTM-No-Permute-FCN Model"
	@echo "	alstm_no_permute_fcn_conv2d: Train Attention-LSTM-No-Permute-FCN-Conv2D Model"
	@echo "	cnn_lstm: Train CNN-LSTM Model"
	@echo "	cnn_lstm_fcn: Train CNN-LSTM-FCN Model"
	@echo "	cnn_lstm_fcn_conv2d: Train CNN-LSTM-FCN-Conv2D Model"
	@echo "	clean: Delete Preprocessed Data"

pr:
	@echo "Preprocess All Feature(132) and Roll, Pitch, Yaw(33) Separately"
	python3 preprocess.py

lstm:
	@echo "Train LSTM Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_lstm.py | tee ./Result/lstm_result.txt
	@echo "Check Result Directory"

fcn:
	@echo "Train FCN Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_fcn.py | tee ./Result/fcn_result.txt
	@echo "Check Result Directory"

fcn_conv2d:
	@echo "Train FCN-Conv2D Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_fcn_conv2d.py | tee ./Result/fcn_conv2d_result.txt
	@echo "Check Result Directory"

lstm_fcn:
	@echo "Train LSTM-FCN Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_lstm_fcn.py | tee ./Result/lstm_fcn_result.txt
	@echo "Check Result Directory"

lstm_fcn_conv2d:
	@echo "Train LSTM-FCN-Conv2D Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_lstm_fcn_conv2d.py | tee ./Result/lstm_fcn_conv2d_result.txt
	@echo "Check Result Directory"

lstm_no_permute_fcn:
	@echo "Train LSTM-No-Permute-FCN Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_lstm_no_permute_fcn.py | tee ./Result/lstm_no_permute_fcn_result.txt
	@echo "Check Result Directory"

lstm_no_permute_fcn_conv2d:
	@echo "Train LSTM-No-Permute-FCN-Conv2D Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_lstm_no_permute_fcn_conv2d.py | tee ./Result/lstm_no_permute_fcn_conv2d_result.txt
	@echo "Check Result Directory"

alstm_fcn:
	@echo "Train Attention-LSTM-FCN Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_alstm_fcn.py | tee ./Result/alstm_fcn_result.txt
	@echo "Check Result Directory"

alstm_fcn_conv2d:
	@echo "Train Attention-LSTM-FCN-Conv2D Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_alstm_fcn_conv2d.py | tee ./Result/alstm_fcn_conv2d_result.txt
	@echo "Check Result Directory"

alstm_no_permute_fcn:
	@echo "Train Attention-LSTM-No-Permute-FCN Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_alstm_no_permute_fcn.py | tee ./Result/alstm_no_permute_fcn_result.txt
	@echo "Check Result Directory"

alstm_no_permute_fcn_conv2d:
	@echo "Train Attention-LSTM-No-Permute-FCN-Conv2D Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_alstm_no_permute_fcn_conv2d.py | tee ./Result/alstm_no_permute_fcn_conv2d_result.txt
	@echo "Check Result Directory"

cnn_lstm:
	@echo "Train CNN-LSTM Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_cnn_lstm.py | tee ./Result/cnn_lstm_result.txt
	@echo "Check Result Directory"

cnn_lstm_fcn:
	@echo "Train CNN-LSTM-FCN Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_cnn_lstm_fcn.py | tee ./Result/cnn_lstm_fcn_result.txt
	@echo "Check Result Directory"

cnn_lstm_fcn_conv2d:
	@echo "Train CNN-LSTM-FCN-Conv2D Model"
	@echo "Before Training, You Must Set GPU_ID_LIST in 'train_{model_name}.py'"
	mkdir -p Result
	python3 train_cnn_lstm_fcn_conv2d.py | tee ./Result/cnn_lstm_fcn_conv2d_result.txt
	@echo "Check Result Directory"

clean:
	@echo "Delete Preprocessed Data"
	rm -rf $(OUTPUT_RPY_DIR)
	rm -rf $(OUTPUT_ALL_DIR)
	rm -rf __pycache__
	rm -rf Result
